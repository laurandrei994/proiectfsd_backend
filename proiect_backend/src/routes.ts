import { TFController } from "./controller/TFController";
import {UserController} from "./controller/UserController";
import { needsAuth, verifyToken } from "./middlewares/authMiddle";
import { checkExistingUser, checkValidUserBodySave, checkValidUserBodyLogin } from "./middlewares/validationMiddle";

export const Routes = [
    {
    method: "get",
    route: "/users",
    controller: UserController,
    action: "all",
    middlewares: [needsAuth, verifyToken],
    }, 
    {
    method: "get",
    route: "/users/:id",
    controller: UserController,
    action: "one",
    middlewares: [needsAuth, verifyToken, checkExistingUser],
    }, 
    {
    method: "put",
    route: "/users/:id",
    controller: UserController,
    action: "update",
    middlewares: [needsAuth, verifyToken, checkExistingUser],
    }, 
    {
    method: "post",
    route: "/users",
    controller: UserController,
    action: "save",
    middlewares: [needsAuth, verifyToken, checkValidUserBodySave],
    }, 
    {
    method: "delete",
    route: "/users/:id",
    controller: UserController,
    action: "remove",
    middlewares: [needsAuth, verifyToken, checkExistingUser],
    },
    {
    method: "post",
    route: "/register",
    controller: UserController,
    action: "register",
    middlewares: [checkValidUserBodySave],
    },
    {
    method: "post", 
    route: "/login",
    controller: UserController,
    action: "login",
    middlewares: [checkValidUserBodyLogin],
    },
    {
    method: "get",
    route: "/users/info",
    controller: UserController,
    action: "getUserInfo",
    middlewares: [verifyToken]
    },
    {
    method: "post",
    route: "/train",
    controller: TFController,
    action:"train"
    },
    {
    method: "post",
    route: "/evaluate",
    controller: TFController,
    action: "evaluate"
    }
];