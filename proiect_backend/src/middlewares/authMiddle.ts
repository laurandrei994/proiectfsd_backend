import { NextFunction, Request, Response} from "express";

var jwt = require("jsonwebtoken");
const jwtSecret = process.env.SECRET_KEY;

export function needsAuth(request: Request, response: Response, next: NextFunction) {
    try {
        let token: String = request.headers.authorization.split(" ")[1];
        if (!jwt.verify(token, process.env.SECRET_KEY)) {
            response.sendStatus(401);
            return;
        }
    } catch (error) {
        console.log("Authorization Header error !! (not present or wrong format)");
        response.sendStatus(401);
        return;
    }
    return next();
}

export function verifyToken(request: Request, response: Response, next: NextFunction) {
    const authentificationToken = request.body.token;
    if (!authentificationToken) {
        response.status(401).send("A token is required for authentification");
        return;
    }
    try {
        const decoded = jwt.verify(authentificationToken, jwtSecret);
        response.user = decoded;
    } catch (err) {
        response.status(401).send("Invalid token");
        return;
    }
    return next;
}