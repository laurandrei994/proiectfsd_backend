import { getRepository } from "typeorm";
import { NextFunction, Request, Response } from 'express';
import { MnistData } from "../tensorflow/data";
import { getModel, train, doPrediction} from '../tensorflow/script';
import * as fileUpload from "express-fileupload"

const tf = require('@tensorflow/tfjs');
require('@tensorflow/tfjs-node');

async function toArrayBuffer(buf) {
    var ab = new ArrayBuffer(buf.length);
    var view = new Uint8Array(ab);
    for (var i = 0; i < buf.length; ++i) {
        view[i] = buf[i];
    }
    return ab;
}

export class TFController {
    async evaluate(request: Request, response: Response, next: NextFunction) {
        try {

            if (!request.files || Object.keys(request.files).length === 0) {
                return response.status(400).send('No files were uploaded.');
            }

            let sampleFile = request.files.sampleFile;

            const model = await tf.loadLayersModel("file://./src/tensorflow/model/model.json");

            const arrayBuffer = await toArrayBuffer(sampleFile.data);

            try{
            var prediction = await doPrediction(model, arrayBuffer);
            }catch(err){
                return response.status(400).send(err);
            }
            
            return response.status(200).send(
                {
                    "prediction": prediction[0]
                });

        } catch (error) {
            console.log(error);
            return response.status(500).send(JSON.stringify(error));
        }
    }

    async train(request: Request, response: Response, next: NextFunction) {
        try {
            const data = new MnistData();
            var model = getModel();

            await data.load();
            const train_results = await train(model, data);
            await model.save("file://./src/tensorflow/model");

            return response.status(200).send(train_results);


        } catch (error) {
            console.log(error);
            return response.status(500).send(error);
        }
    }


}