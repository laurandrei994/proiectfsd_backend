import {getRepository} from "typeorm";
import * as jwt from "jsonwebtoken";
import {NextFunction, Request, Response} from "express";
import {User} from "../entity/User";

export class UserController {

    private userRepository = getRepository(User);

    async all(request: Request, response: Response, next: NextFunction) {
        return this.userRepository.find();
    }

    async one(request: Request, response: Response, next: NextFunction) {
        return this.userRepository.findOne(request.params.id);
    }

    async save(request: Request, response: Response, next: NextFunction) {
        return this.userRepository.save(request.body);
    }

    async update(request: Request, response: Response, next: NextFunction) {
        let userToUpdate = await this.userRepository.findOne(request.params.id);

        const {
            userName = userToUpdate.userName,
            email = userToUpdate.email,
            password = userToUpdate.password,
        } = request.body;

        return await this.userRepository.save({
            id: userToUpdate.id,
            userName,
            email,
            password,
        });
    }

    async remove(request: Request, response: Response, next: NextFunction) {
        let userToRemove = await this.userRepository.findOne(request.params.id);
        await this.userRepository.remove(userToRemove);
        return `User with id ${request.params.id} has been removed`;
    }

    async register(request: Request, response: Response, next: NextFunction) {
        let userExists = (
            await this.userRepository.find({
                where: [
                    {userName: request.body.userName},
                    {email: request.body.email },
                ],
            })
        ).length > 0;
        if (userExists) {
            response.status(409);
            return "There is already an user with this username or this email";
        }
        this.userRepository.save(request.body);
        return "Successful Register";
    }

    async login(request: Request, response: Response, next: NextFunction) {
        let user = (
            await this.userRepository.find({
                where: { 
                    email: request.body.email, 
                    password: request.body.password },
            })
        ).shift();
        if (!user) {
            response.status(401);
            return 'Incorrect email and/or password';
        }
        response.status(200);
        return {
            token: jwt.sign(
                {data: user.id,},
                process.env.SECRET_KEY,
                {expiresIn: '1h'}
            ),
        };
    }

    async getUserInfo(request: Request, response: Response, next: NextFunction) {
        console.log(response);
        console.log(request);
    }
}