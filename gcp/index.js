const { Datastore } = require("@google-cloud/datastore");
const datastore = new Datastore({
	projectId: 'proiectfsdgcloud',
	keyFilename: 'datastore-credential.json'
});

const kind = 'Schedule';

// https://us-central1-proiectfsdgcloud.cloudfunctions.net/getSchedule
exports.getSchedule = async (req, res) => {
	res.set('Access-Control-Allow-Origin', '*');
    const query = datastore.createQuery(kind);
    await datastore.runQuery(query)
	.then((scheduleList) => {
        res.send(JSON.stringify(scheduleList[0]));
    });
};

// https://us-central1-proiectfsdgcloud.cloudfunctions.net/postSchedule
exports.postSchedule = async (req, res) => {
	res.set('Access-Control-Allow-Origin', '*');
	let start = req.query.start_date || req.body.start_date;
    let end = req.query.end_date || req.body.end_date;

	if (!start && !end) {
        return res.status(500).send("Invalid fields!");
    }

	let isValidStartDate = Date.parse(start);
	let isValidEndDate = Date.parse(end);
    if (isNaN(isValidStartDate)||isNaN(isValidEndDate)) {
		return res.status(500).send("This is not a valid date format.");
    }   
  
	const scheduleKey = datastore.key([kind]);
	const scheduleTask = {
		key: scheduleKey,
		data: {
			start_date: start,
			end_date: end
		},
	  };

	datastore.save(scheduleTask)
    .then(() => {
     return res.status(200).send(JSON.stringify(scheduleTask));
    })
    .catch(err => {
		res.status(500).send(JSON.stringify(err));
		console.error('ERROR:', err);
	 	return;
    });

}

